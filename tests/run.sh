#!/bin/bash

# Sanity
set -u

SCRIPT_DIR=$(dirname "$0")
CASES="$SCRIPT_DIR/case*"
DIFF="diff --color"
FAILING=()

readcsv="$1"

for testCase in $CASES
do
  inputFile="$testCase/*.csv"
  expectedOutput="$testCase/output*.txt"
  actualOutput="$testCase/actual_output.txt"

  echo "-------- Running Test Case $testCase"

  cat $inputFile | $readcsv > $actualOutput
  $DIFF $actualOutput $expectedOutput
  exitStatus=$?
  if [ $exitStatus -ne 0 ]
  then
    FAILING+=($testCase)
  fi

  printf "\n"
done

if [ "${#FAILING[@]}" -gt "0" ]
then
  echo "Some Tests Failed"
  echo "${FAILING[*]}"
  exit 1
fi

exit 0
