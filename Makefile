CFLAGS=-Wall -Wextra -pedantic -std=c99

all: readcsv test

readcsv: readcsv.o
	$(CC) -o readcsv readcsv.o

clean:
	rm readcsv{,.o}

test: readcsv
	./tests/run.sh ./readcsv

.PHONY: clean test
