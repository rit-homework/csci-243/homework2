// Aggregates information about census data.
//
// Written by: Martin Charles (mxc4400@rit.edu)

#include <stdio.h> // fgets
#include <stdlib.h> // strtol, EXIT_SUCCESS
#include <stdbool.h> // fake booleans
#include <limits.h> // LONG_MAX

int main(void) {
    // The assignment says that no line will be longer than 256 characters but
    // it doesn't specify whether that includes the newline. Let's assume it
    // doesn't to be safe.
    int buffer_size = 256 + 1 + 1;
    char buffer[buffer_size];

    // The total number of zip codes.
    int zip_code_count = 0;

    // The sum of all populations.
    long int sum_total_population = 0;

    // The population of the smallest city.
    long int lowest_population = LONG_MAX;

    // The zip code of the smallest city.
    long int lowest_population_zip_code = 0;

    // The population of the largest city.
    long int highest_population = -1;

    // The zip code of the largest city.
    long int highest_population_zip_code = 0;

    // A flag variable to skip the first line.
    bool first = true;

    // Read until EOF.
    while(fgets(buffer, buffer_size, stdin) != NULL) {
      // Skip the first line.
      if (first) {
        first = false;
        continue;
      }

      // The following are the column headers in order:
      // * Zip Code
      // * Total Population
      // * Median Age
      // * Total Males
      // * Total Females
      // * Total Households
      // * Average Household Size

      // Read Zip Code Storing End Pointer
      char *endPtr;
      long int zip_code = strtol(buffer, &endPtr, 10);

      // Skip Comma After Column
      endPtr += sizeof(char);

      // Read Population
      long int total_population = strtol(endPtr, NULL, 10);

      // Update Aggregates
      if (total_population < lowest_population) {
        lowest_population = total_population;
        lowest_population_zip_code = zip_code;
      }

      if (total_population > highest_population) {
        highest_population = total_population;
        highest_population_zip_code = zip_code;
      }

      sum_total_population += total_population;
      zip_code_count++;
    }

    // Report Information
    printf("Total population across %d zipcodes is %ld.\n",
      zip_code_count, sum_total_population);

    printf("The fewest people live in %ld, population %ld.\n",
      lowest_population_zip_code, lowest_population);

    printf("The most people live in %ld, population %ld.\n",
      highest_population_zip_code, highest_population);

    return EXIT_SUCCESS;
}
